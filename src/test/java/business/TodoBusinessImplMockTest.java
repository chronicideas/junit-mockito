package business;

import data.api.TodoService;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.*;

public class TodoBusinessImplMockTest {

    private TodoService todoServiceMock = mock(TodoService.class);
    private TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(todoServiceMock);

    private List<String> todos = Arrays.asList("Learn Spring MVC", "Learn Spring", "Learn to Dance", "Learn magic");

    private List<String> retrieveTodos(String user) {
        given(todoServiceMock.retrieveTodos("Thomas Knee")).willReturn(todos);
        return todoBusinessImpl.retriveTodosRelatedToSpring(user);
    }



    @Test
    public void testDeleteNonSpringTasks_ArgumentCaptor() {
        ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);

        retrieveTodos("Thomas Knee");
        todoBusinessImpl.deleteTodosNotRelatedToSpring("Thomas Knee");

        then(todoServiceMock).should(times(2)).deleteTodo(stringArgumentCaptor.capture());
        then(todoServiceMock).should(never()).deleteTodo("Learn Spring");
        then(todoServiceMock).should(never()).deleteTodo("Learn Spring MVC");
        assertThat(stringArgumentCaptor.getAllValues().size(), is(2));
    }

    @Test
    public void testRetriveTodosRelatedToSpring_CheckSize() {
        assertEquals(2, retrieveTodos("Thomas Knee").size());
    }

    @Test
    public void testRetriveTodosRelatedToSpring_EmptySize() {
        assertEquals(0, retrieveTodos("Mr Empty").size());
    }

    @Test
    public void testRetrieveTodosRelatedToSpring_CheckExactMatches() {
        for (String filteredTodo: retrieveTodos("Thomas Knee")) {
            assertTrue(filteredTodo.contains("Spring"));
        }
    }
}