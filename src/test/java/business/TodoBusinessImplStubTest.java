package business;

import data.api.TodoService;
import data.api.TodoServiceStub;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class TodoBusinessImplStubTest {

    private TodoService todoServiceStub = new TodoServiceStub();
    private TodoBusinessImpl todoBusinessImpl = new TodoBusinessImpl(todoServiceStub);

    private List<String> filteredTodos = todoBusinessImpl.retriveTodosRelatedToSpring("Thomas Knee");

    @Test
    public void testRetriveTodosRelatedToSpring_CheckSize() {
        assertEquals(2,filteredTodos.size());
    }

    @Test
    public void testRetrieveTodosRelatedToSpring_CheckExactMatches() {
        for (String filteredTodo:filteredTodos) {
            assertTrue(filteredTodo.contains("Spring"));
        }
    }
}