package business;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.*;

public class ListTest {

    private List listMock = mock(List.class);

    @Test
    public void testMockListSize() {
        given(listMock.size()).willReturn(2);
        int listSize = listMock.size();
        assertEquals(2, listMock.size());
    }

    @Test
    public void testMockListSize_ReturnMultipleValues() {
        given(listMock.size()).willReturn(2).willReturn(3).willReturn(4);

        assertEquals(2, listMock.size());
        assertEquals(3, listMock.size());
        assertEquals(4, listMock.size());
    }

    @Test
    public void testMockListGet() {
        given(listMock.get(anyInt())).willReturn("TestifyQA");

        assertThat("TestifyQA", is(listMock.get(0)));
    }

    @Test
    public void testMockListGet_ReturnNull() {
        assertNull(listMock.get(0));
    }

    @Test(expected=RuntimeException.class)
    public void testListMockGet_ThrowException() {
        given(listMock.get(anyInt())).willThrow(new RuntimeException("ERROR: YOU HAVE FUCKED UP!"));
        listMock.get(1);
    }

    @Test(expected=RuntimeException.class)
    public void testListMockGet_MixingUpThrowException() {
        given(listMock.subList(anyInt(), 5)).willThrow(new RuntimeException("Something went wrong"));
        listMock.get(4);
    }
}
